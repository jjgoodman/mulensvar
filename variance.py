# Calculate variance of flux for a prescribed spatial power spectrum using
# tabulated flux kernel \hat J(\omega).
# Version of 8/14/2013 by JJG

# Import standard modules
from numpy import *
import pickle
import glob
from scipy import interpolate

# The global variable(s) "source_params" may be used by the image-source function
# Uncomment one of these to use DexterAgol() function
#source_params = (0.3605, 1.9063, -2.503) # Dexter & Agol disk with \sigma_T= 0
#source_params = (0.3725, 1.3694, -2.435)  # Dexter & Agol disk with \sigma_T= 0.3
source_params = (0.4015, 0.9628, -2.339) # Dexter & Agol disk with \sigma_T= 0.5
#source_params = (0.3444, 0.5775, -2.345) # Dexter & Agol disk with \sigma_T= 0.7

#===================================================================================
#   DEFINITION OF IMAGE SOURCE TYPES AND FUNCTIONS
#===================================================================================
# A function "source(omega_x,omega_y,scale)" must be defined to return the spatial
# power spectrum of the source, i.e. the 2D Fourier transform of the unlensed
# surface brightness:
#
# P(\omega) = |\int \tilde I(\vec\omega)\exp(i\vec\omega\cdot\vec\theta d^2\vec\theta |^2
#
# We normalize P(0) to unity so that variance.py computes the flux variance normalized
# by the square of the mean macrolensed flux.
#
# Below are two representative source functions.
# Define your own function, then add its name to the list "source_funcs" under
# "IMMMEDIATE EXECUTABLES" below.

def gaussian(wx,wy,scale=1): # Spatial power spectrum of the source: Gaussian disk.
    # Inputs: wx, wy == cartesian components of wavevector \omega
    #         scale  == scale factor for overall size of source.
    return exp(-0.5*(wx**2+wy**2)*scale**2)

def DexterAgol(wx,wy,scale=1): # Three-parameter fit to inhomogeneous disks of
                               # Dexter & Agol (2011)
    # Inputs: wx, wy == cartesian components of wavevector \omega
    #         scale  == scale factor for overall size of source.
    core,alpha,beta = source_params
    wscaled = sqrt(wx**2+wy**2)*scale
    return (1.+(wscaled/core)**alpha)**(beta/alpha)

#===================================================================================
# Utility routines
#===================================================================================
def unfold(x): # Extend a function from 1st quadrant (0 to pi) to all 4 quadrants,
               # imposing reflection symmetry around x and y axes
    n = len(x)
    y = zeros(4*n-3)
    y[:n] = x
    y[n:2*n-1] = x[-2::-1]
    y[2*n-1:4*n-3] = y[1:2*n-1]
    return y

def onesize(source_size): # Find the variance for the given source size
                          # by a double integral against the lensing kernel
    if source_size < small_size:
        fluxvar = small_var + small_dvar*log(small_size/source_size)
    elif source_size > big_size:
        fluxvar = big_var*(source_size/big_size)**big_dvar
    else:
        fw = zeros(len(ww))
        if source_size <= 1. : # Integrate in angle, then in radius, at the knots w[:]
            for i in range(len(w)):
                aint = array([source(w[i]*csp[k],w[i]*snp[k],scale=source_size) for k in range(np)])\
                *unfold(Jtab[i,:])
                # Fit a periodic (per=1) cubic (k=3) unsmoothed (s=0) spline to the angular dependence:
                tck = interpolate.splrep(phi,aint,per=1,k=3,s=0.) 
                fw[i+1] = w[i]* interpolate.splint(0.,2*pi,tck)
            # Do the radial integral also by splines:
            tck = interpolate.splrep(ww,fw,k=3,s=0.)
            fluxvar = interpolate.splint(0.,ww[-1],tck) /(2*pi)**2
        else: # Integrate in radius, then in angle
            ws = ww/source_size # Rescale the length of \omega to fit the transform of the source.
            fp = zeros(np)  # This will hold the results of the radial integrals at each angle phi
            for k in range(np):
                if k < na:  # Jtab and Jtck are tabulated for angles in 1st quadrant only, so for
                    kk = k  # other quadrants (k >= na), we have to "unfold" the index
                elif k < 2*na-1:
                    kk = 2*na-2-k
                elif k < 3*na-2:
                    kk = k - (2*na-2)
                else:
                    kk = np-1-k
                fw = interpolate.splev(ws,Jtck[kk])*ws*\
                   array([source(ws[i]*csp[k],ws[i]*snp[k],scale=source_size) for i in range(len(ws))])
                tck   = interpolate.splrep(ws,fw,k=3,s=0.)
                fp[k] = interpolate.splint(ws[0],ws[-1],tck)
            # Now do the angular integral, using a periodic spline:
            tck = interpolate.splrep(phi,fp,per=1,k=3,s=0.)
            fluxvar = interpolate.splint(0.,2*pi,tck)/(2*pi)**2
             
    return fluxvar

#===============================================================================================
# IMMEDIATE EXECUTABLES
#===============================================================================================
if __name__ == '__main__':
    print '\nWelcome to variance.py, which computes the normalized microlensed flux variance'
    print 'for a given spatial power spectrum of the source.\n'

    # Select source type
    source_funcs = [gaussian,DexterAgol] # List of pointers to the functions
    source_names = [str(foo).split()[1] for foo in source_funcs] # List of function names
    ns = len(source_funcs)
    print 'Choose one of these source types:'
    for j in range(ns):
        print '{:d}: '.format(j) + source_names[j]
    js = int(raw_input('\nYour choice [0 through {:d}]: '.format(ns-1)))
    source = source_funcs[js]

    # Select which tabulation of the microlensing kernel to use
    pkfiles = glob.glob('Jtab*pkl')
    print '\nThe following tabulations of the lensing kernel (computed with outer.py) are available:'
    for i in range(len(pkfiles)):
        print '{:d}: '.format(i) + pkfiles[i]
    file_index = int(raw_input('\nWhich should I use (allowed range: {:d} to {:d}) ? '.\
                               format(0,len(pkfiles)-1)))
    print 'Using file '+pkfiles[file_index]

    # Read in the file
    f = open(pkfiles[file_index],'r')
    (kappa,kappa_star,gamma,sigma10,w,a,Jtab) = pickle.load(f)
    print '\n Parameters: \n'
    print 'kappa      = {:.4f}'.format(kappa) + ' (convergence)'
    print 'kappa_star = {:.4f}'.format(kappa_star) + ' (convergence due to stars)'
    print 'gamma      = {:.4f}'.format(gamma) + ' (shear)'
    print 'sigma10    = {:.4f}'.format(sigma10) + ' (width of log-normal mass fct., in dex)'
    print '\nTable runs from {:.2e} to {:.2e} in |omega| at {:d} angles from 0 to pi/2'.\
        format(w[0],w[-1],len(a))

    # Prepare work arrays for use by function "onesize"
    # The kernel is tabulated in angle from 0 to pi/2 with respect to the shear axis;
    # extend the angles to [0, 2\pi]:
    na = len(a); np = 4*na-3
    phi = zeros(np)
    phi[:na] = a
    phi[na:2*na-1] = pi - a[-2::-1]
    phi[2*na-1:4*na-3] = phi[1:2*na-1] + pi
    csp = cos(phi); snp = sin(phi)
    # Since w[0] > 0, make room for a radial point to represent the origin:
    ww = zeros(len(w)+1); ww[1:] = w

    # Fit Jtab to splines at each angle, for use with larger sources when we integrate
    # radially before azimuthally:
    Jtck = []
    Jext = zeros(len(ww))
    for i in range(na):
        Jext[1:] = Jtab[:,i]; Jext[0] = Jext[1]
        tck = interpolate.splrep(ww,Jext,k=3,s=0.)
        Jtck.append(tck)

    # Prepare asymptotics
    # Threshold for overresolved sources
    big_size = 1./(3*w[0])
    # Threshold for "pointlike" sources
    small_size = 3./w[-1]
    big_var = onesize(big_size)
    big_dvar= log(big_var/onesize(big_size/2.))/log(2.)  # Powerlaw slope for large sources
    small_var  = onesize(small_size)
    small_var2 = onesize(small_size*2.)
    small_dvar = (small_var-small_var2)/log(2.) # Coefficient of log for small sources

    # Select a range of source sizes:
    print '\nNow we will select a range of source sizes.'
    print 'The size is the radius [r] in the (smooth) disk at which hc/kT(r) = observing wavelength.'
    print 'The unit of length is the radius of the Einstein ring of an average star.\n'
    min_size = float(raw_input('Enter minimum source radius (positive, nonzero): '))
    max_size = float(raw_input('Enter maximum source radius: '))
    nsizes   = int(raw_input('Enter number of sizes: '))

    # Calculate and print the sizes:
    scales = logspace(log10(min_size),log10(max_size),nsizes)
    fvars  = zeros(len(scales)) 
    print '\n Source size     Variance'
    for i in range(nsizes):
        fvars[i] = onesize(scales[i])
        print '{:12.4e}   {:12.4e}'.format(scales[i],fvars[i])
    


    

