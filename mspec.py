# Version of 8/12/2013 by JJG
#
# Imported by outer.py
#
# Smooth the tabulated H function over a (Miller-Scalo/Chabrier) mass spectrum:
#
# The mass function $\xi(\log m) d(\log m)$ is originally defined as the number of
# stars per unit volume with masses in the logarithmic range d(\log m). N.B.: common log.
# Chabrier (2003) also defines the "system mass function" to include unresolved binaries.
# He gives the functional form
#
# \xi(\log m) = C_0 \exp[ -(\log m - \log m_0)^2/2\sigma^2]
#
# For the system mass function, he recommends m_0 = 0.22 Msun and \sigma = 0.57 dex.
# Note that the logs above are common logs: for natural logs, one must multiply
# sigma by \ln(10).
#
# Based on this, we define the normalized mass spectrum such that f(x)\dx gives
# the fraction of all stars with \ln(m/m_0) in the range x to x+ dx, and
#
# f(x) = \exp[-x^2/2\sigma^2] /\sqrt{2\pi\sigma^2}
#
# BY DEFAULT, we take \sigma = 0.3 \times \ln(10) \approx 0.691
# This is narrower than Chabrier's function.  Our reason is that
# masses greater than Msun must have been formed "recently", and so
# this part of the MF may depend upon the star formation rate of the lens galaxy.
# In fact Chabrier tacks on a powerlaw for disk stars above 1 Msun.  But we would
# like to have m_0 as the only mass scale, and also a simple functional
# form, so we compromise by choosing a smaller \sigma so as to suppress high-mass
# stars.  In particular, if m_0 = 0.22 Msun and \sigma = 0.3 dex, then only 1.4% of
# stars will have masses above 1 Msun.
#
# We work in units such that the mean mass is unity: Since
#   \langle m\rangle = m_0 \int_{-\infty}^\infty  e^x f(x) dx = m_0 \exp(\sigma^2/2),
# this means that we take m_0 = \exp(-\sigma^2/2)
#
from numpy import *
import pickle

# Mass spectrum.  Alter as desired.
def mass_spec(x,sigma):
    return exp(-0.5*(x/sigma)**2)/sqrt(2*pi*sigma**2)

# Set coefficients for asymptotic behaviors at small and large beta, assuming that
# htable was computed for a single lens mass.
#
def asymp(htable,lnb):
    nh = shape(htable)[1]
    c = zeros(nh)
    eulerconst = 0.5772156649  # a.k.a. \gamma
    c0l = pi/4.                                    # Coef. of \beta^2\ln\beta in h_0
    c1l = pi/16.                                   # Coef. of \beta^3\ln\beta in h_1
    c[0] = (pi/4.)* (eulerconst - 1.  -log(8.))    # Coef. of \beta^2         in h_0
    c[1] = (pi/16.)*(eulerconst -0.75 -log(8.))    # Coef. of \beta^3         in h_1
    c[2] = pi/8.                                   # Coef. of \beta^2 in h_2
    for n in range(3,nh):
        c[n] = htable[0,n]*exp(-n*lnb[0])          # Coef. of \beta^n in h_n [should be \pi/64 for n=3]
    # Determine coefficients for large-beta expansions
    d = zeros(nh)
    d[0]= -pi                                     # Coef. of \beta in h_0 at large \beta
    d[1]= -pi                                     # Coef. of \beta in h_1 at large \beta
    d11 =  pi                                     # Coef. of 1 in h_1 (subdominant term)
    d[2] = 3*pi/4.
    for n in range(3,nh):
        d[n] = d[n-1]*(2*n-1.)*(2*n-3.)/(2.*n)    # Coef of \beta^{1-n} in h_n

    return (c0l,c1l,c,d,d11)

def msmooth(sigma10 = 0.3): # We use 0.3 in place of Chabrier(2003)'s 0.57 for "systems"
    sigma = log(10.)*sigma10  # Convert the width to natural log

    # Read in the table for H(\beta)
    try:
        hfile = open('htable.pkl','r')
    except IOError :
        print 'ERROR: htable.pkl [table of h_n(\beta)] not found.  Run hseries.py first '
    (lnb,htable) = pickle.load(hfile)
    hfile.close()
    nh = shape(htable)[1]
    print 'mspec.msmooth: htable is {:d} by {:d}; lnb[0]={:f} lnb[-1]={:f}'.\
      format(shape(htable)[0],nh,lnb[0],lnb[-1]) #DEBUG
    #    
    # Get coefficients for small-beta expansions
    (c0l,c1l,c,d,d11) =  asymp(htable,lnb)
    #
    if sigma10 <= 0 : # A negative value means to skip the mass-spectrum smoothing
        return (htable,lnb,c0l,c1l,c,d,d11)
    #
    #
    # Extrapolate the tabulated h functions so as to minimize
    # boundary effects when we convolve with the mass function:
    #
    dlb = lnb[1]-lnb[0]  # We assume uniform spacing in \ln\beta
    more = int(round(max(4*sigma,log(10.))/dlb))
    nb = len(lnb)
    lnbext = zeros(nb+2*more)
    lnbext[more:nb+more] = lnb[:]
    lnbext[:more]    = linspace(lnb[0]-more*dlb,lnb[0]-dlb,more)
    lnbext[nb+more:] = linspace(lnb[-1]+dlb,lnb[-1]+more*dlb,more)
    hext = zeros((nb+2*more,nh))
    hext[more:nb+more,:] = htable[:,:]
    for i in range(more):
        hext[i,0] = exp(2*lnbext[i])*(c0l*lnbext[i] + c[0])
        hext[i,1] = exp(3*lnbext[i])*(c1l*lnbext[i] + c[1])
        for n in range(2,nh):
            hext[i,n] = exp(n*lnbext[i])*c[n]
    lbnyq = - log(dlb/pi)
    nyq   = int(round((lbnyq-log(pi)-lnbext[0])/dlb))
    # The oscillations of h_n(\beta) are critically sampled
    # for \log\beta \approx lnbext[nyq] + \ln(\pi).
    for i in range(nyq,nb+2*more):
        # Make a smooth transition between tabulated & asymptotic values:
        wgt = 0.5*(1. + tanh(2*(lnbext[i] - lbnyq))) 
        hext[i,0] = wgt*d[0]*exp(lnbext[i]) + (1.-wgt)*hext[i,0]
        hext[i,1] = wgt*(d[1]*exp(lnbext[i]) +d11) + (1.-wgt)*hext[i,1]
        for n in range(2,nh):
            hext[i,n] = wgt*d[n]*exp((1.-n)*lnbext[i]) + (1.-wgt)*hext[i,n]
    #
    # Convolve with mass spectrum
    #
    mu = -0.5*sigma**2 # Reduced characteristic mass so that MEAN mass is unity
    ms = zeros(2*more+1)
    for i in range(2*more+1):
        xm = (i-more)*dlb
        ms[i] = mass_spec(xm-mu,sigma)
    ms = ms/sum(ms)
    rms = ms[-1::-1] # Reverse the mass function to use "convolve" properly below
    hbar = zeros(shape(htable))
    [nnb,nnh] = shape(htable)
    for n in range(nh):
        hbar[:,n] = convolve(hext[:,n],rms,'valid')
    #
    # Adjust asymptotic coefficients for smoothing with mass spectrum:
    # Coefficients at small beta:
    c0l  = c0l *exp(sigma**2)                      # Coeff. of \beta^2\ln\beta in h_0
    c[0] = c[0]*exp(sigma**2)   + 1.5*sigma**2*c0l # Coeff. of \beta^2         in h_0
    c1l  = c1l *exp(3*sigma**2)                    # Coeff. of \beta^3\ln\beta in h_1
    c[1] = c[1]*exp(3*sigma**2) + 2.5*sigma**2*c1l # Coeff. of \beta^3         in h_1
    c[2] = c[2]*exp(sigma**2)                      # Coeff. of \beta^2         in h_2
    for n in range(3,nh):
        c[n] = c[n]*exp(n*(n-1)*sigma**2/2.)       # Coefficient of \beta^n in h_n

    # Coefficients at large beta; note that the coefficients of \beta^0 and \beta^1 are unchanged
    # because the zeroth and first moments of the mass spectrum are normalized to unity:
    for n in range(2,nh):
        d[n] = d[n]*exp(n*(n-1)*sigma**2/2.)
    return (hbar,lnb,c0l,c1l,c,d,d11)

