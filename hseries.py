# Version of 8/12/2013 by JJG
#
# Calculate the coefficients h_n(\beta) in the expansion
#
# H(\beta_x,\beta_y) = \sum_{n=0}^{\infty} i^n h_n(\beta) exp(in\psi)
#
# where i=\sqrt{-1}, \psi = arctan(\beta_y,\beta_x), and \beta =\sqrt{\beta_x^2 + \beta_y^2}.
#
# Explicitly, in terms of the Laplace coefficients b_{1/2}^{(n)}(r),
# h_0(\beta) = (\pi/2)\int_0^\infty [J_0(\beta r) -1]           b_{1/2}^{(0)}(r) dr/r^2
# h_1(\beta) =    \pi \int_0^\infty [J_1(\beta r) - \beta r/2 ] b_{1/2}^{(1)}(r) dr/r^2
# h_n(\beta) =    \pi \int_0^\infty J_n(\beta r)                b_{1/2}^{(n)}(r) dr/r^2   for n > 1

# Notice that the factor i^n has been omitted from these definitions so that the h_n are real.

# Import modules
# First, standard modules
from numpy import *
from scipy import special
import pickle
import argparse
import time
# Personal modules
import myfunc as m   # myfunc.py is used for complex-valued Legendre functions of the 2nd kind.


# Points and weights for quadratures of the form \int_a^b f(x) dx = \sum_k w_k f[a+(b-a)x_k]
# when f(x) = (c_0 + c_1 x)\log(x)
xquad = array([0.0882968651, 0.6751864909])
wquad = array([0.2984998937, 0.7015001063])

# Calculate Laplace coefficients b_{1/2}^{(n)}(r) for integer n and complex r, using
# b_{1/2}^{(n)}(r) = \pi^{-1}\int_0^{2\pi} d\theta \frac{\cos n\theta}{\sqrt{1+r^2-2r\cos\theta}}
#                  = 2/(\pi\sqrt{r}) Q_{n -1/2}((1+r^2)/(2r))
# For convenience, OMIT THE FACTOR (1/\pi), because we'd just have to multiply by \pi in h_n
#
def lapcoef(n,r): # Calculate the coefficients for 0,1,2,...,n
    if r == 0 :
        B = zeros(n+1)
        B[0] = 2*pi
    else :
        z = (1.+r**2)/(2*r)
        B = (2/sqrt(r))*m.Qhint(n,z)
    return B

def lc_large_n(n,r): # Asymptotic approximation to the Laplace coefficient valid for n >> 1.
    z = sqrt((r-1.)**2/r)
    return special.kn(0,n*z)*2/sqrt(r)

def bvals(beta, n, r): # Evaluate integrand at points r[:]
    nstep = len(r)
    f = zeros(nstep)
    if n == 0 :
        if r[0] == 0 :
            f[0] = - lapcoef(1,0.)[0]*beta**2 /8.
        else :
            f[0] = 0.5*lapcoef(1,r[0])[0]*(special.jn(0,beta*r[0])-1.)/r[0]**2
        for k in range(1,nstep):
            f[k] = 0.5*lapcoef(1,r[k])[0]*(special.jn(0,beta*r[k])-1.)/r[k]**2
    elif n == 1:
        if r[0] != 0 :
            f[0] = lapcoef(1,r[0])[1]*(special.jn(1,beta*r[0])-0.5*beta*r[0])/r[0]**2
        for k in range(1,nstep):
            f[k] = lapcoef(1,r[k])[1]*(special.jn(1,beta*r[k])-0.5*beta*r[k])/r[k]**2
    else :
        if r[0] != 0 :
            f[0] = lapcoef(n,r[0])[n]*special.jn(n,beta*r[0])/r[0]**2
        for k in range(1,nstep):
            f[k] = lapcoef(n,r[k])[n]*special.jn(n,beta*r[k])/r[k]**2
    return f

def hvals(beta, n, r): # Evaluate complex integrand with Hankel functions
    nstep = len(r)
    f = zeros(nstep,dtype=complex)
    if n == 0 :
        for k in range(nstep):
            B = lapcoef(1,r[k])
            f[k] = 0.5*B[0]*(special.hankel1(0,beta*r[k])-1.)/r[k]**2
    elif n == 1:
        for k in range(nstep):
            B = lapcoef(1,r[k])
            f[k] =    B[1]*(special.hankel1(1,beta*r[k])-0.5*beta*r[k])/r[k]**2
    else :
        for k in range(nstep):
            B = lapcoef(n,r[k])
            f[k] =    B[n]*special.hankel1(n,beta*r[k])/r[k]**2
    return f

def simpson(nstep): # Construct weights for Simpson's rule: (1/3,4/3,2/3,4/3,...,2/3,4/3,1/3)
    w = array([ 2*(1 + k%2) for k in range(nstep)])
    w[0] = w[-1] = 1
    return w/3.

# Calculate h_n(\beta) for n >= 0 :
def rnint(beta, n, stepsize = 0.1):
    zcrit = max(2*n/exp(1.), 1.)
    zerojn = special.jn_zeros(n,1)[0]  # First zero of J_n(x)
    zeroyn = special.jn_zeros(n,1)[0]  # First zero of Y_n(x)

    if beta < zerojn :  # Bessel fct. not yet oscillating at singularity r=1
        # Integrate almost to r = 1, with uniform steps in y == -ln(1-r) so that r = 1-exp(-y):
        rmax = 1. - stepsize**3
        smax = -log(1.-rmax)
        nstep = 2*int(round(smax/stepsize)) + 1
        s = linspace(0.,smax,nstep)
        ds = s[1]-s[0]
        delr = exp(-s)
        r = 1. - delr; r[0] = 0
        wgt = simpson(nstep)*delr*ds # i.e., dr = ds*(dr/ds)
        f = bvals(beta,n,r)
        rsum = sum(wgt*f)
        rnow = r[-1]
        # Integrate on both sides of logarithmic singularity at r=1:
        delr = 1.-rnow
        r = 1. - delr*xquad
        wgt =    delr*wquad
        f = bvals(beta,n,r)
        rsum += sum(wgt*f) # Low-order quadrature for logarithmic singularity
        rnow = 1. + delr
        r = 1. + delr*xquad
        f = bvals(beta,n,r)
        rsum += sum(wgt*f) # Low-order quadrature for logarithmic singularity

        # Integrate to a safe distance beyond the singularity
        rmax = max(zerojn/beta,2.)
        smin = log(rnow-1.); smax = log(rmax-1.)
        nstep = 2*int(round((smax-smin)/stepsize)) + 1
        s = linspace(smin,smax,nstep); ds = s[1]-s[0]
        delr = exp(s)
        r = 1. + delr
        wgt = simpson(nstep)*delr*ds
        f = bvals(beta,n,r)
        rsum += sum(wgt*f)
        rnow = rmax
            
    else : # Integrand starts to oscillate before the singularity.
        rmax = zeroyn/beta # Note 1st zero of Y_n(x) precedes 1st zero of J_n(x)
        nstep = 2*int(round(zeroyn/stepsize)) + 1
        r = linspace(0.,rmax,nstep); dr = r[1]-r[0]
        wgt = simpson(nstep)*dr
        f = bvals(beta,n,r)
        rsum = sum(wgt*f)
        rnow = r[-1]
        # Integrate toward r=1 via the UHP, using Hankel fct.
        if beta < 20. :
            # integrate along a semicircle using uniform steps in
            # s = \ln\sinh(\beta\theta), so that d\theta/ds = \beta^{-1}\tanh(\beta\theta)
            radius = (1.-rnow)/2. # radius of semicircle.
            center = (1.+rnow)/2.
            smax = log(sinh(beta*pi)); smin = log(sinh(beta*stepsize**3))
            nstep = 2*int(round((smax-smin)/stepsize)) + 1
            s = linspace(smax,smin,nstep); ds = s[1]-s[0]
            theta = arcsinh(exp(s))/beta
            delr =  radius*exp((1.j)*theta)
            r = center + delr
            dr = (1.j)*ds*delr*tanh(beta*theta)/beta
            wgt = simpson(nstep)*dr
            f = hvals(beta,n,r)
            rsum += sum(real(wgt*f))
            rnow = r[-1]
        else :  # Use a rectilinear path
            # First go straight up (along imag(r)):
            dr = stepsize/beta; dist = min(20./beta,0.5)
            nstep = 2*int(round(dist/dr)) + 1
            r = rnow + (1.j)*linspace(0.,dist,nstep); dr = r[1]-r[0]
            wgt = simpson(nstep)*dr
            f = hvals(beta,n,r)
            rsum += sum(real(wgt*f))
            xmax = 0.999*sqrt(1.-dist**2)
            rnext = xmax + dist*(1.j) # This is just inside the unit circle            
            if n < 2 : # Go across, toward unit circle, integrating subtracted terms
                rnow = r[-1]
                smin = log(rnow); smax = log(rnext)
                nstep = 2*int(round(abs(smax-smin)/stepsize)) + 1
                s = linspace(smin,smax,nstep); ds = s[1]-s[0]
                r = exp(s); dr = r*ds
                wgt = simpson(nstep)*dr
                f = hvals(beta,n,r)
                rsum += sum(real(wgt*f))
                rnow = real(r[-1])
            # Now go diagonally down, staying within the unit circle:
            rnow = rnext
            smin = -log(1.-rnow)
            smax = -log(stepsize**3)
            ds = stepsize*min(1., 4./abs(beta*(1.-rnow)))
            nstep = 2*int(round(abs(smax-smin))/ds) + 1
            s = linspace(smin,smax,nstep); ds = s[1]-s[0]
            r = 1. - exp(-s)
            dr = exp(-s)*ds
            wgt = simpson(nstep)*dr
            f = hvals(beta,n,r)
            rsum += sum(real(wgt*f))
            rnow = real(r[-1])
        # Integrate over logarithmic singularity at r=1:
        delr = 1.-rnow
        r = 1. - delr*xquad
        wgt =    delr*wquad
        f = hvals(beta,n,r)
        rsum += real(sum(wgt*f)) # Low-order quadrature for logarithmic singularity
        delr = abs(delr); rnow = 1. + delr
        r = 1. + delr*xquad
        wgt =    delr*wquad
        f = bvals(beta,n,r)
        rsum += sum(wgt*f)      # Low-order quadrature for logarithmic singularity
        
        # Integrate along real(r) to a safe distance from r=1.
        rmax = rnow + 2./beta
        smin = log(rnow-1.)
        smax = log(rmax-1.)
        nstep = 2*int(round((smax-smin)/stepsize)) + 1
        s = linspace(smin,smax,nstep); ds = s[1]-s[0]
        delr = exp(s)
        r = 1. + delr
        wgt = simpson(nstep)*delr*ds
        f = bvals(beta,n,r)
        rsum += sum(real(wgt*f)) + real(delr[0]*f[0])
        rnow = rmax

    # Next, starting at r > 1 on the real axis, integrate obliquely in the
    # upper-half complex r plane, replacing J_n(\beta r) with the Hankel function
    # H_n^{(1)}(\beta r), which decreases exponentially in the UHP and has J_n as its real part.
    dist = 10*sqrt(2.)/beta
    dr = stepsize*min(1./beta,abs(rnow-1.))
    nstep = 2*int(round(dist/dr)) + 1
    vector = exp((1.j)*pi/4)
    r = linspace(rnow, rnow+vector*dist,nstep); dr = r[1]-r[0]
    wgt = simpson(nstep)*dr
    f = hvals(beta,n,r)
    rsum += sum(real(wgt*f))
    rnow = r[-1]

    # Finally, if n == 0 or 1, complete the integration of the subtracted terms
    if (n == 0 or n == 1) and abs(rnow) < 90 :
        smax = log(200./abs(rnow))
        nstep = max(2*int(round(smax/stepsize)) +1, 3)
        s = linspace(0.,smax,nstep); ds = s[1]-s[0]
        r = rnow*exp(s)
        wgt = simpson(nstep)*r*ds
        if n == 0 :
            for k in range(nstep):
                B = lapcoef(1,r[k])
                rsum += real( -0.5*wgt[k]*B[0]/r[k]**2 )
            rsum += real( -0.25*B[0]/r[k])  # Approximate integral to infinity, since b_{1/2}^{(0)}(r) \propto 1/r
        else : # n == 1
            for k in range(nstep):
                B = lapcoef(1,r[k])
                rsum += real( -0.5*beta*wgt[k]*B[1]/r[k] )
            rsum += real( -0.25*beta*B[1] )   # Approximate integral to infinity, since b_{1/2}^{(1)}(r) \propto 1/r^2

    return rsum

# Make the table for the inner integral

def tabulate(beta_min = 1e-3, beta_max = 1e3, dln_beta = 0.1, nmax = 10):
    lbmin = log(beta_min); lbmax = log(beta_max)
    nb = int(round(abs((lbmax-lbmin)/dln_beta))) + 1
    lnb = linspace(lbmin,lbmax,nb)
    htable = zeros((nb,nmax+1),dtype=double)
    for ib in range(nb):
        beta = exp(lnb[ib])
        for n in range(nmax+1):
            htable[ib,n] = rnint(beta,n)
        if ib % 10 == 0 :  # show progress
            print '...done up to beta <= {0:15.7e}'.format(beta)

    # Write out the results, both to a pickle file and to a plain ASCII one
    pkl_file = open('htable.pkl','w')
    pickle.dump((lnb,htable),pkl_file)
    pkl_file.close()
    txt_file = open('htable.txt','w')
    for ib in range(nb):
        line = '{0:12.8f}'.format(lnb[ib])
        for n in range(nmax+1):
            line = line + '{0:15.7e}'.format(htable[ib,n])
        print >>txt_file, line
    txt_file.close()
    print 'Pickled table is stored in htable.pkl'
    print 'Column-separated values in htable.txt'

# Prompt for input
def getit(var,pstring):
    val = raw_input('Enter '+pstring+' [default: '+str(var)+']: ')
    if val == '':
        x = var
    else:
        t = type(var)
        if t == int:
            x = int(val)
        elif t == float:
            x = float(val)
        elif t == complex:
            x = complex(val)
        elif t == str:
            x = s
        else:
            print 'WARNING: Cannot parse the input string '+val+'. Using default.'
            x = var
    return x

#========================================================================
# Immediate executables
#========================================================================

if __name__ == '__main__':
    tstart = time.time()
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--default',action='store_true',default=False,help=\
                        'use defaults rather than prompt for parameters')
    args = vars(parser.parse_args())

    # Establish defaults
    beta_min = 1e-3  # minimum value of independent variable for tabulation
    beta_max = 1e3   # maximum value
    dln_beta = 0.01  # tabulation interval, in natural log of indep. var.
    nmax     = 7     # Maximum n for harmonics h_n(\beta) to be tabulated.
    savem = (beta_min,beta_max,dln_beta,nmax) # Just in case we need the defaults

    # Get parameters, if defaults are not to be used
    if args['default'] == False :
        beta_min = getit(beta_min,'minimum beta')
        beta_max = getit(beta_max,'maximum beta')        
        dln_beta = getit(dln_beta,'step size in natural log')
        nmax     = getit(nmax,'maximum harmonic')

    # Make some sanity checks
    if beta_min < 0 :
        print 'beta_min must be positive. Using {:e}'.format(savem[0])
        beta_min = savem[0]
    elif beta_max <= beta_min :
        print 'beta_max must be > beta_max. Using {:e}'.format(10*beta_min)
    elif dln_beta < 0:
        print 'Step size must be positive. Using {:f}'.format(savem[2])
        dln_beta = savem[2]
    elif nmax < 0 or nmax > 20:
        print 'Maximum harmonic must be between 0 and 20. Using {:d}'.\
            format(savem[3])
        nmax = savem[3]

    # Display parameters
    print '\n A table will be built with these parameters:\n'
    print '    beta_min = {:e}'.format(beta_min)
    print '    beta_max = {:e}'.format(beta_max)
    print '    dln_beta = {:f}'.format(dln_beta)    
    print '    nmax     = {:d}'.format(nmax)
    # Now make the table
    print '\n OK, starting the table. This may take a while...'
    tabulate(beta_min, beta_max, dln_beta, nmax)
    tused = time.time() - tstart
    print 'All done.  Elapsed time = {:f} seconds.'.format(tused)

