# Special functions needed for the inner integral
# Version of 8/12/2013 by JJG

# Import modules
from numpy import *

tiny = 1e-20

# Evaluate elliptic integrals K(z) and E(z) using
# the process of the arithmetic and geometric means.
# The parameter z (== square of the modulus) may be complex.
# See Abramowitz and Stegun, sec. 17.6
def myellipke(z):
    azm1 = abs(1.-z)
    if azm1 == 0 :
        raise ValueError('Argument to myellipke cannot be exactly 1.')
    elif azm1 < 0.1 :  # Use A&S 17.3.29 if z is close to 1
        rz = sqrt(1.-z)
        zp = ((1-rz)/(1+rz))**2
        [Kp,Ep] = myellipke(zp)
        K = 2*Kp /(1+rz)
        E = (1+rz)*Ep - 2*rz*Kp/(1+rz)
    else :
        eps = 1e-12
        a = 1.
        b = sqrt(1.-z)
        c = sqrt(z)
        csum = z
        cfac = 1
        while abs(a-b) > eps:
            c = (a-b)/2;
            cfac = 2*cfac
            csum += cfac*c**2
            am = (a+b)/2;
            bm = sqrt(a*b);
            a = am
            b = bm
        K = pi/(a+b)
        E = K*(1. - csum/2.)
    return (K,E)

# Evaluate K(z) and E(z) as hypergeometric series.
# This converges only if abs(z) < 1.
def myellipseries(z):
    if abs(z) >= 1 :
        raise ValueError('Argument to myellipseries > 1.')
    eps = 1e-16
    n = 0
    kterm = 1; eterm = 1
    ksum = kterm; esum = eterm
    while abs(kterm) > eps:
        n += 1
        kterm = kterm *z*(double(2*n-1)/(2*n))**2
        ksum += kterm
        esum += -kterm/(2*n-1)
    K = (pi/2)*ksum; E = (pi/2)*esum
    return (K,E)

# Legendre functions of 2nd kind and half-integer degree.
# Returns n+1 values: $Q_{-1/2}(z)$ through $Q_{n-1/2}$
def Qhint(n,z):
    if z == 1 or z == -1 :
        print 'Qhint received z = %f' % n
        raise ValueError('Second argument to Qhint cannot be 1 or -1')
    zm = 2./(z+1)
    rzm = sqrt(zm)
    [K,E] = myellipke(zm)
    Q = [rzm*K]             # This is Q_{-1/2}(z)
    Q.append( rzm*(z*K - (z+1)*E) ) # Q_{+1/2}(z)
    if n > 1 :
        if real(z) >= 0:
            fac = z + sqrt(z**2 -1.);
        else:
            fac = z - sqrt(z**2 -1.);
        if 2*n*log(abs(fac)) < 5 : # Use forward recursion if this won't lose too much precision
            for k in range(1,n):   # Apply recursion relation A&S 8.5.3
                Q.append( (2*k*z*Q[k]-(k-0.5)*Q[k-1])/(k+0.5) )  # Q_{k +1/2}(z)
        else:  # Use backward recursion starting from large n
            nn = n + int(round(0.5*log(1./tiny)/log(abs(fac))))
            if type(z)==complex :
                Qt = zeros(nn+1,dtype=complex)
            else :
                Qt = zeros(nn+1)
            Qt[nn-1] = fac; Qt[nn] = 1.
            k = nn-1
            while k > 0:
                Qt[k-1] = (2*k*z*Qt[k] - (k+0.5)*Qt[k+1])/(k-0.5)
                k = k-1
            rat = Q[0]/Qt[0]
            for k in range(2,n+1):
                Q.append(rat*Qt[k])            
    return array(Q)

    
    

    
