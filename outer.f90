module ai
  ! Fortran routines for outer integral.  Use with outer.py
  implicit none
  !
  ! Global variables and arrays to be set externally, e.g. by outer.py :
  !
  integer          :: nh     ! Number of azimuthal harmonics
  double precision :: kappa=0.45, kappa_star=0.045, gamma=0.45 ! Macrolensing parameters
  double precision :: aerr=1d-8, rerr=1d-8      ! Accuracy parameters for quadratures
  double precision :: betamin=1d-3, betamax=1d3 ! Least and largest \beta for tabulation of h_n(\beta)
  double precision,allocatable,dimension(:)    :: bt  ! knots for spline fits to h_n(\beta)
  double precision,allocatable,dimension(:,:)  :: hsp ! spline coefficients for  h_n(\beta)
  double precision,allocatable,dimension(:)    :: smallb, bigb  ! Used for small-beta extrapolation
  double precision :: smallb0l=0d0, smallb1l=0d0                ! Used for small-beta extrapolation
  !
  ! Stuff local to this module
  double precision  :: PI = 2*asin(1d0)  ! f2py chokes on the "parameter" attribute, apparently
  integer, parameter :: NHMAX=20
  double precision, dimension(0:NHMAX-1) :: hfuncs

contains

  subroutine getcoefs(beta)
    ! evaluate the coefficients of cos(n\psi) in the Fourier
    ! series for H(\beta,\psi), excepting the factors of i**n
    implicit none
    double precision,intent(in) :: beta
    ! Local variables:
    integer :: k=3,ier,n
    double precision :: b1mn
    double precision,dimension(1) :: bval

    if (beta < betamin) then ! Use small-beta expansions
       hfuncs(0) = (smallb0l*log(beta) + smallb(0))*beta**2
       hfuncs(1) = beta**3 *(smallb1l*log(beta) +smallb(1))  
       do n = 2,nh-1
          hfuncs(n) = smallb(n+1)*beta**n  ! Unfortunately, because they are allocated externally,
          ! arrays smallb and bigb are assumed to start at 1 not 0.
       enddo
    else if (beta > betamax) then ! Use large-beta expansion
       hfuncs(0) = bigb(1)*beta            ! Again, bigb(n+1) corresponds to the nth harmonic
       hfuncs(1) = bigb(2)*(beta - 1d0)
       b1mn = 1d0
       do n = 2,nh-1
          b1mn = b1mn/beta  ! beta**(1-n)
          hfuncs(n) = bigb(n+1)*b1mn
       enddo
    else ! Interpolate in the table
       k = 3
       bval(1) = beta ! 4th argument to mysplev has to be an array
       do n = 0,nh-1
          call mysplev(bt,hsp,k,bval,hfuncs,ier)
       enddo
    endif
  end subroutine getcoefs

  subroutine angsamp(alpha,beta,udt,nufac,a0,na,rsum)
    implicit none
    double precision,intent(out) :: rsum
    double precision, intent(in) :: alpha,beta,udt,nufac,a0
    integer, intent(in) :: na
    ! Local variables:
    integer :: i, k
    double precision :: da,angle,csa,csevn,csodd,hrsum,hisum,sgn
    ! Executables
    da = 2*PI/na
    rsum = 0
    do i = 1,na
       angle = a0 + (i-1)*da
       csa = cos(angle)
       csevn = 1.d0
       csodd = csa
       hrsum = 0; hisum = 0 ! Will hold real & imaginary parts of argument of exponential
       sgn = 1d0
       do k = 0,nh-2,2 ! This loop generates cos(k*angle) by recursion on k,
          ! which may not be the best strategy for a multithreaded processor
          hrsum = hrsum + hfuncs(k)  *csevn*sgn
          hisum = hisum + hfuncs(k+1)*csodd*sgn
          csevn = 2*csa*csodd - csevn
          csodd = 2*csa*csevn - csodd
          sgn = - sgn
       enddo
       rsum = rsum + cos(nufac*hisum - udt*cos(angle-alpha))*exp(nufac*hrsum)
    enddo
    return
  end subroutine angsamp

  subroutine angint(dtheta,w,u,alpha,est,ntot)
    ! Do the integral over the azimuth of \Delta\theta
    implicit none
    double precision, intent(in)  :: dtheta, w, u, alpha
    double precision, intent(out) :: est   ! estimate of the integral
    integer, intent(out)          :: ntot  ! number of samples of integrand
    ! Local variables:
    integer :: na
    double precision :: beta, udt, nufac, a0, da, est1, est2, rsum
    beta = 4*w/dtheta
    udt = u*dtheta
    nufac = (kappa_star/PI)*(dtheta/2)**2
    call getcoefs(beta)
    da = 2*PI/nh
    a0 = da
    na = nh
    call angsamp(alpha,beta,udt,nufac,a0,na,rsum)
    est1 = rsum*da
    a0 = a0/2
    call angsamp(alpha,beta,udt,nufac,a0,na,rsum)
    est2 = rsum*da
    est = (est1+est2)/2
    do while (abs(est1-est2) > rerr*(abs(est1)+abs(est2)) )
       est1 = est; da = da/2; na = na*2; a0 = a0/2
       call angsamp(alpha,beta,udt,nufac,a0,na,rsum)
       est2 = rsum*da
       est = (est1+est2)/2.
    enddo
    ntot = 2*na
    return
  end subroutine angint

  subroutine mysplev(t,c,k,x,y,ier)
    !  subroutine mysplev evaluates at a number of points x(i),i=1,2,...,m
    !  a spline s(x) of degree k, given in its b-spline representation.
    !  Modified for fortran 90 and double precision by JJG from original SPLEV by Paul Dierckx.
    !
    !  This modified version assumes multiple (nf) sets of spline coefficients c(:,1:nf)
    !  that share the same knot values t(:).
    !
    !  inputs:
    !    t    : array,length n, which contains the position of the knots.
    !    c    : array, shape n by nf, which contains the b-spline coefficients.
    !           Note that even if nf=1, c must be a two-dimensional array.
    !    k    : integer, giving the degree of s(x).
    !    x    : array,length m, of the points where s(x) must be evaluated.
    !
    !  outputs:
    !    y    : array, shape m by nf, of the interpolated values for each of the nf splines.
    !    ier  : error flag
    !      ier = 0 : normal return
    !      ier =10 : invalid input data (see restrictions)
    !
    !  restrictions:
    !    m >= 1
    !    t(k+1) <= x(i) <= x(i+1) <= t(n-k) , i=1,2,...,m-1.
    !
    !  other subroutines required: fpbspl.
    !
    !  references :
    !    de boor c  : on calculating with b-splines, j. approximation theory
    !                 6 (1972) 50-62.
    !    cox m.g.   : the numerical evaluation of b-splines, j. inst. maths
    !                 applics 10 (1972) 134-149.
    !    dierckx p. : curve and surface fitting with splines, monographs on
    !                 numerical analysis, oxford university press, 1993.
    !
    !  author :
    !    p.dierckx
    !    dept. computer science, k.u.leuven
    !    celestijnenlaan 200a, b-3001 heverlee, belgium.
    !    e-mail : Paul.Dierckx@cs.kuleuven.ac.be
    !
    !  latest update : march 1987
    !
    !  ..inputs
    integer, intent(in) :: k
    double precision, intent(in) :: t(:), c(:,:), x(:)
    !  ..outputs
    integer, intent(out) :: ier
    double precision, dimension(size(x),size(c,2)), intent(out) :: y
    !  ..local variables
    integer :: n,m,nf,i,j,k1,l,l1,nk1,ix
    double precision :: arg,tb,te, h(6)
    !  ..
    !db #ifdef DEBUG
    !db     !  before starting computations a data check is made. if the input data
    !db     !  are invalid control is immediately repassed to the calling program.
    !db     ier = 10
    !db     if (m<1) return
    !db     do i=2,m
    !db        if(x(i).lt.x(i-1)) return
    !db     enddo
    !db #endif
    ier = 0
    n = size(t); m = size(x); nf = size(c,2)
    !  fetch tb and te, the boundaries of the approximation interval.
    k1 = k+1
    nk1 = n-k1
    tb = t(k1)
    te = t(nk1+1)
    l = k1
    l1 = l+1
    !  main loop for the different points.
    do ix=1,m
       !  fetch a new x-value arg.
       arg = x(ix)
       if(arg.lt.tb) arg = tb
       if(arg.gt.te) arg = te
       !  binary search for knot interval t(l) <= arg < t(l+1)
       if (arg<tb) then
          arg = tb; l = k1
       else if (arg>te) then
          arg = te; l = nk1
       else
          l = k1; l1 = max(nk1,l+1)
          do while (l1 > l+1)
             i = (l+l1)/2 ! Note l < i < l1
             if (arg .ge. t(i)) then
                l = i
             else
                l1 = i
             end if
          end do
       end if
       !  evaluate the non-zero b-splines at arg.
       call fpbspl(t,k,arg,l,h)
       !  Interpolate all nf functions at x=arg 
       forall(j = 1:nf) y(ix,j) = dot_product(h(1:k1), c((l-k):l, j))
    end do
    return
  end subroutine mysplev

  subroutine fpbspl(t,k,x,l,h)
    !subroutine fpbspl evaluates the (k+1) non-zero b-splines of
    !degree k at t(l) <= x < t(l+1) using the stable recurrence
    !relation of de boor and cox.
    !Modified for fortran90 from original in dierckx.f
    !
    !..scalar arguments..
    double precision,intent(in) :: x
    integer,intent(in) :: k,l
    !..array arguments..
    double precision,dimension(:),intent(in)  :: t
    double precision,dimension(6),intent(out) :: h
    !..local scalars..
    double precision :: f,tli,tlj
    integer :: i,j
    !..local arrays..
    double precision :: hh(5)
    !
    h(1) = 1d0
    do j=1,k
       do i=1,j
          hh(i) = h(i)
       end do
       h(1) = 0d0
       do i=1,j
          tli = t(l+i)
          tlj = t(l+i-j)
          f = hh(i)/(tli-tlj)
          h(i) = h(i)+f*(tli-x)
          h(i+1) = f*(x-tlj)
       end do
    end do
    return
  end subroutine fpbspl

  subroutine check() ! Make sure that everything is initialized
    if (allocated(hsp)) then
       nh = size(hsp,2)
    else
       print *,'WARNING: SPLINE COEFFICIENT ARRAY out.mod.hsp HAS NOT BEEN SET'
    endif
    if(.not.allocated(bt)) then
       print *,'WARNING: SPLINE KNOT ARRAY out.mod.bt HAS NOT BEEN SET'
    endif
    if(.not.allocated(smallb).or..not.allocated(bigb)) then
       print *,'ASYMPTOTIC COEFFICIENTS out.mod.smallb OR out.mod.bigb HAVE NOT BEEN SET'
    end if
    if(smallb0l.eq.0d0) then
       print *,'COEFFICIENT OF SMALL-BETA LOG TERM out.mod.smallb0l HAS NOT BEEN SET'
    endif
  end subroutine check

end module ai
