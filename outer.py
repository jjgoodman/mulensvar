# Calculate the outer (dtheta) integral using previously tabulated H(\beta)
# This is based on "newthint.py" but allows for a mass spectrum and other improvements.
# Version of 8/12/2013 by JJG
# Corrections Jan 2014 by JJG
#
# Standard modules:
from numpy import *
from scipy import interpolate
from scipy import integrate
import pickle
# Local modules:
import mspec  # For smoothing over a "Chabrier" mass spectrum
import ai     # Fortran-based module for angular integral

# Numerical parameters (these control the accuracy of the results)

rerr = 1e-8    # Relative error for angular integration
aerr = 1e-8    # Absolute error for angular integration
oscerr = 1e-4  # Relative error for Euler summation: don't make this too small!
neumax = 100   # Maximum number of half cycles of radial integrand

def getcoefs(beta): # evaluate the coefficients of cos(n\psi) in the Fourier
    # series for H(\beta,\psi), including the factors of i**n
    if use_ai == True:
        raise NameError('CALL ai.ai.getcoefs NOT getcoefs when use_ai==True !')
    hfuncs = zeros(nh,dtype=complex)
    if beta < betamin : # Use small-beta expansions
        hfuncs[0] = (c0l*log(beta) + c[0])*beta**2
        hfuncs[1] = beta**3 *(c1l*log(beta) +c[1])*(1.j)
        for n in range(2,nh):
            hfuncs[n] = c[n]*((1.j)*beta)**n
    elif beta > betamax : # Use large-beta expansion
        hfuncs[0] = d[0]*beta
        hfuncs[1] = d[1]*(1.j)*(beta - 1.)
        for n in range(2,nh):
            hfuncs[n] = d[n]*beta**(1-n)*(1.j)**n
    else: # Interpolate in the table
        z = 1. + 0.j
        for n in range(nh):
            hfuncs[n] = interpolate.splev(beta,(bt,hsp[:,n],3),der=0)*z
            z = z*(1.j)
    return hfuncs

def norm(v):  # For some reason, this script can't find the numpy version
    return sqrt(dot(v,v))

def get_param(wvec) :
    w = norm(wvec)         # Length of the 2-vector \omega
    uvec = dot(wvec,Mmat)  # M\cdot\omega, M == magnification matrix.
    u = norm(uvec)         # Length of M\cdot\omega
    wdotu  = dot(uvec,wvec)
    wxu    = wvec[0]*uvec[1] - wvec[1]*uvec[0]
    alpha = math.atan2(wxu,wdotu)  # Angle from \omega to M\cdot\omega
    return(w,u,alpha)

def angsamp(alpha,udt,nufac,hfuncs,da,a0): # Sample the angular integrand at intervals da starting from a0
    na = int(round(2*pi/da))
    a = a0 + linspace(0., 2*pi, na, endpoint=False)
    f = zeros(len(a),dtype=double)
    for i in range(len(a)):
        angle = a[i]
        h = sum([hfuncs[n]*cos(n*angle) for n in range(nh)])
        f[i] = real(exp( -(1.j)*udt*cos(angle-alpha) + nufac*h))
    return f
        
def angint(dtheta,param): #Integrate adaptively over the angle between vectors dtheta and omega.
    # For smooth periodic functions, the error of the midpoint or trapezoidal rule decreases
    # exponentially as the number of sampling points increases.  There is no benefit to using
    # a higher-order scheme such as Simpson's rule.  We use the midpoint rule here.
    # The fortran version ai.ai.angint does the same but much faster
    #
    if use_ai == True:
        raise NameError('CALL ai.ai.angint NOT angint when use_ai==True !')
    (w,u,alpha) = param[:]
    nufac = nu*(dtheta/2)**2
    udt = u*dtheta
    beta = 4*w/dtheta
    hfuncs = getcoefs(beta)
    da = 2*pi/nh
    a0 = da
    est1 = sum(angsamp(alpha,udt,nufac,hfuncs,da,a0))*da # Midpoint rule with stepsize da
    a0 = a0/2
    est2 = sum(angsamp(alpha,udt,nufac,hfuncs,da,a0))*da # Same stepsize, but samples offset by half a step
    est = (est1+est2)/2. # Equivalent to midpoint rule with stepsize da/2
    # Successively refine the mesh by factors of 2 until errors are small enough:
    while abs(est1-est2) > rerr*(abs(est1)+abs(est2)) :
        est1 = est; da = da/2; a0 = a0/2
        est2 = sum(angsamp(alpha,udt,nufac,hfuncs,da,a0))*da    
        est = (est1+est2)/2.
    return est

def eugrand(dtheta,param):
    if use_ai == True :
        (w,u,alpha) = param[:]
        (val,nsamp) = ai.ai.angint(dtheta,w,u,alpha)
    else :
        val = real(angint(dtheta,param))
    return val*dtheta

# Apply Euler summation to the alternating series obtained by doing the radial
# integral over successive half cycles of the oscillatory factor 
# \exp(-j\omega\cdot{M}\cdot\Delta\theta):
def eulint(wvec):
    param = array(get_param(wvec))
    (w,u,alpha) = param[:]
    lobe = pi/u
    estab = [[]]
    y = 0.
    mmax = 3
    for k in range(mmax):
        [val,error] = integrate.quad(eugrand,k*lobe,(k+1)*lobe,args=param)
        y += val
        estab[0].append(y)
    for m in range(1,mmax):
        estab.append([]) # Start a new row of terms
        for k in range(mmax-m):
            estab[m].append((estab[m-1][k]+estab[m-1][k+1])/2.)
    # Add more half periods until convergence
    while mmax < neumax and abs(estab[-2][0]-estab[-2][1]) > oscerr*abs(estab[-1][0]) :
        m = mmax
        [val,error] = integrate.quad(eugrand,m*lobe,(m+1)*lobe,args=param)
        y += val
        estab[0].append(y)
        osc = abs((estab[-2][0]-estab[-2][1])/estab[-1][0])
        mmax += 1
        estab.append([])
        for m in range(1,mmax):
            estab[m].append((estab[m-1][-2]+estab[m-1][-1])/2.)
        if mmax == neumax-1 :
            print 'WARNING: slow convergence at omega = {:12.4e}, alpha={:12.4e}'\
                .format(w,alpha)
        
    result = estab[-1][0]*abs(detM) # We multiply rather than divide by detM to get the
                                    # magnification variance normalized by the square of
                                    # of the mean magnification, and M.M. = 1/det(M)
    return (result,estab)

# Fit splines to the coefficient functions
# Call this AFTER "(hbar,lnb,c0l,c1l,c,d,d11) = mspec.msmooth(sigma10)"

def fithn():
    nh = shape(hbar)[1]  # The number of azimuthal harmonics
    # Extend the grid to \beta<0 using symmetry, to better
    # approximate the desired behavior near \beta=0.
    pad = min(32,len(lnb)) # Number of "ghost" points at \beta < 0.
    bext = zeros(len(lnb)+pad)
    hext = zeros(len(bext))
    bext[pad:] = exp(lnb)
    bext[pad-1::-1] = -bext[pad:2*pad]
    hext[pad:] = hbar[:,0]
    hext[pad-1::-1] = hext[pad:2*pad]    
    tck = interpolate.splrep(bext,hext,s=0)
    bt = tck[0]  # spline knots
    hsp = zeros((len(tck[1]),nh))
    hsp[:,0] = tck[1] # spline coefficients for 0th harmonic
    for n in range(1,nh):
        hext[pad:] = hbar[:,n]
        hext[pad-1::-1] = hext[pad:2*pad]    
        if n%2 == 1 : # odd harmonics should be odd in \beta.
            hext[:pad] = -hext[:pad]
        tck = interpolate.splrep(bext,hext,s=0)
        hsp[:,n] = tck[1]   # coefficients for nth harmonic
    return nh,bt,hsp

# Calculate the variance kernel J(\omega)(det|M|)^2 -1  for a range of \omega
# Control parameters:
# wmin  == minimum of magnitude of \omega
# wmax  == maximum |\omega|
# dlw   == logarithmic stepsize for |\omega|
# na    == number of samples for angle between \omega and major axis of the
#          magnification matrix S;  (0 <= angle <= pi/2)
#
def make_J_table(wmin=0.1, wmax=30.00, dlw=0.1, na=9): # Note default parameters
    asw1 = arcsinh(wmin);  asw2 = arcsinh(wmax)
    nw = int(round((asw2-asw1)/dlw)) + 1
    w = sinh(linspace(asw1,asw2,nw))
    rat = log(abs(Mmat[0,0]/Mmat[1,1]))
    if abs(rat) < log(2.) : # Eigenvalues are not too different; uniform spacing in angle:
        a = linspace(0., pi/2, na)
    elif rat < 0 : # Use nonuniform angular samples concentrated toward axis 0
        a = (pi/2)*(1. - cos(linspace(0., pi/2, na)))
    else :         # Use nonuniform angular samples concentrated toward axis 1
        a = (pi/2)*sin(linspace(0., pi/2, na))
    Jtab = zeros((nw,na),dtype=double)
    wa,Japp,Jlo,Jhi = make_Japp(wmin,wmax,dlw,na)
    for i in range(nw):
        for k in range(na):
            wvec = w[i]*array([cos(a[k]),sin(a[k])])
            [result, estab] = eulint(wvec)
            if result > 0 :
                Jtab[i,k] = result
            else:
                Jtab[i,k] = Japp[i,k]
            #print 'omega={:11.4e}, alpha={:7.4f}: J = {:12.5e}'.format(w[i],a[k],Jtab[i,k])
        if i%10 == 0: print 'Completed up to omega={:11.4e}'.format(w[i])
    return (w,a,Jtab)

# Construct an analytic approximation using asymptotic formulae
def make_Japp(wmin=0.1, wmax=30.0, dlw= 0.1, na=9):
    asw1 = arcsinh(wmin);  asw2 = arcsinh(wmax)
    nw = int(round((asw2-asw1)/dlw)) + 1
    w = sinh(linspace(asw1,asw2,nw))
    a = linspace(0., pi/2, na)
    Japp = zeros((nw,na),dtype=double)
    Jlo = zeros((nw,na),dtype=double)    
    Jhi = zeros((nw,na),dtype=double)
    fac = 2*pi*kappa_star*abs(detM)
    lofac = 2*fac*exp((sigma10*log(10.))**2) # Correction for log-normal mass spectrum
    for i in range(nw):
        for k in range(na):
            wvec = w[i]*array([cos(a[k]),sin(a[k])])
            uvec = dot(Mmat,wvec)
            pvec = uvec + kappa_star*wvec; psq = dot(pvec,pvec)
            Jlo[i,k] = lofac*(dot(uvec,wvec)/dot(uvec,uvec))**2
            Jhi[i,k] = fac*w[i]/((kappa_star*w[i])**2 + psq)**1.5
            if min(Jlo[i,k],Jhi[i,k]) == 0. :
                Japp[i,k] = 0.
            else :
                Japp[i,k] = 1./(1./Jlo[i,k] + 1./Jhi[i,k])
    return (w,Japp,Jlo,Jhi)

# Prompt for input
def getit(var,pstring):
    val = raw_input('Enter '+pstring+' [default: '+str(var)+']: ')
    if val == '':
        x = var
    else:
        t = type(var)
        if t == int:
            x = int(val)
        elif t == float:
            x = float(val)
        elif t == complex:
            x = complex(val)
        elif t == str:
            x = s
        else:
            print 'WARNING: Cannot parse the input string '+val+'; using default.'
            x = var
    return x


################################################################################
# Immediately executed statements
################################################################################
if __name__ == '__main__':

    # Test whether the fortran module has been compiled
    try:
        use_ai = True
        foo = open('ai.so','r'); foo.close(); 
    except:
        use_ai = False
        print 'Cannot find module ai. Perhaps you forgot to run f2py on ai.f90,'
        print 'or perhaps you do not have f2py or fortran.'
        print 'I will run anyway, but VERY SLOWLY....'

    # Set default parameters
    sigma10    = 0.3   # Dispersion of log-normal mass function, in dex,
                       # i.e. common log rather than natural log.
    kappa      = 0.45  # Total macrolensing convergence
    kappa_star = 0.045 # Stellar contribution to macrolensing convergence
    gamma      = 0.45  # Macrolensing shear
    wmin       = 0.03  # Minimum length of wavevector \omega in units \theta_Einstein = 1
    wmax       = 30.   # Maximum length
    dlw        = 0.1   # Step size in arcsinh(length)
    na         = 17    # Number of angles per quadrant for direction of wavevector
    savem = (sigma10,kappa,kappa_star,gamma,wmin,wmax,dlw,na)

    # Get parameters
    print '\n****************************************************************************'
    print 'Welcome to outer.py, which tabulates the microlensing flux-variance kernel J'
    print '\nWe will now select parameters. Use empty lines to accept defaults.\n'
    print '****************************************************************************'
    sigma10 = getit(sigma10,'dispersion of log-normal mass spectrum')
    kappa =   getit(kappa,'total convergence')
    kappa_star = getit(kappa_star,'stellar convergence')
    gamma =   getit(gamma,'macrolensing shear')
    print '\n*****************************************************************************'
    print '\nNow we select parameters for the tabulation of J.'
    print 'Recall that wavevector omega is the Fourier dual to sky position vector theta'
    print 'in units where the Einstein ring radius is unity\n'
    print '*******************************************************************************'
    wmin = getit(wmin,'minimum length of wavevector')
    wmax = getit(wmax,'maximum length')
    dlw  = getit(dlw,'step size in arcsinh(length)')
    na   = getit(na,'number of directions for wavevector')

    # Sanity checks
    if kappa < 0:
        print 'Negative convergence is unphysical.  Using default.'
        kappa = savem[1]
    elif kappa_star < 0:
        print 'Negative stellar convergence is unphysical.  Using default.'
        kappa_star = savem[2]
    elif kappa_star > kappa:
        print 'Negative dark-matter convergence is unphysical. Setting to zero.'
        kappa = kappa_star
    elif wmin < 0:
        print 'Minimum length must be non-negative. Using default.'
        wmin = savem[4]
    elif wmax <= wmin:
        print 'Maximum length must be greater than minimum. Using 10*wmin'
        wmax = 10*wmin
    elif dlw <= 0:
        print 'Step length must be positive. Using default.'
        dlw = savem[6]
    elif na <= 0:
        print 'Must sample at least one angle. Using default.'
        na = savem[7]

    # Show the input parameters
    print '\nMacrolensing parameters chosen:\n'
    print 'sigma      = {:f}'.format(sigma10)
    print 'kappa      = {:f}'.format(kappa)
    print 'kappa_star = {:f}'.format(kappa_star)
    print 'gamma      = {:f}'.format(gamma)
    print '\nParameters for tabulation of the kernel:\n'
    print 'omega_min         = {:e}'.format(wmin)
    print 'omega_max         = {:e}'.format(wmax)
    print 'd(arcsinh(omega)) = {:f}'.format(dlw)
    print 'Number of angles  = {:d}'.format(na)

    # Compute derived parameters
    detM = (1.-kappa)**2 - gamma**2
    if abs(detM) < 1e-2 :
        print 'WARNING: macro magnification > 100. Kernel may not converge.'
    nu = kappa_star/pi # Stars per unit solid angle when \theta_E = 1.
    Mmat = array([[1.-kappa-gamma,0.], [0.,1.-kappa+gamma]]) # Magnification matrix
    print '\nConvolving with mass spectrum'
    (hbar,lnb,c0l,c1l,c,d,d11) = mspec.msmooth(sigma10) # Mass spectrum
    betamin = exp(lnb[0]); betamax = exp(lnb[-1])

    if use_ai == True: # Initialize the fortran module
        ai.ai.nh, ai.ai.bt, ai.ai.hsp = fithn()
        ai.ai.kappa      = kappa
        ai.ai.kappa_star = kappa_star
        ai.ai.gamma      = gamma
        ai.ai.betamin = betamin
        ai.ai.betamax = betamax
        ai.ai.smallb   = c
        ai.ai.smallb0l = c0l
        ai.ai.smallb1l = c1l
        ai.ai.bigb     = d
        ai.ai.init()
    else:
        nh,bt,hsp = fithn()

    # Make the table
    print 'Computing the table....'
    w,a,Jtab = make_J_table(wmin,wmax,dlw,na)

    # Store it in a pickle file
    outfile = 'Jtab_k={:.3f},ks={:.3f},g={:.3f},sig={:.3f}.pkl'.format(kappa,kappa_star,gamma,sigma10)
    pkl_file = open(outfile,'w')
    pickle.dump((kappa,kappa_star,gamma,sigma10,w,a,Jtab),pkl_file)
    pkl_file.close()
    print 'All done.\nPickled table stored in '+outfile
