! For use with rays.py, as an alternative to "binrays2".
module br
   implicit none
contains
   subroutine binrays(x,y,xsize_source,ysize_source,nx,ny,cells)
      ! Arguments
      double precision, dimension(:,:), intent(in) :: x, y
      double precision, intent(in) :: xsize_source, ysize_source
      integer, intent(in) :: nx,ny
      double precision, dimension(0:nx-1,0:ny-1), intent(out):: cells
      ! Local variables:
      double precision :: dx,dy,fx,fy,wx0,wy0,wxm,wym
      integer :: n1,n2,i1,i2,ix,iy,jx,jy,jxm,jym
      integer, dimension(2) :: dims

      dims = shape(x)
      n1 = dims(1)
      n2 = dims(2)
      !print *,'binrays.f90: arrays x and y are ',n1,' by ',n2 !DEBUG
      !print *,'binrays.f90: the array cells is ',nx,' by ',ny !DEBUG
      dx = xsize_source/nx
      dy = ysize_source/ny
      cells = 0
      do i1 = 1,n1
      do i2 = 1,n2
         fx = x(i1,i2)/dx
         ix = ceiling(fx)
         jx = modulo(ix,nx); jxm = modulo(ix-1,nx)
         wxm = ix-fx; wx0 = 1d0-wxm
         fy = y(i1,i2)/dy
         iy = ceiling(fy)
         jy = modulo(iy,ny); jym = modulo(iy-1,ny)
         wym = iy-fy; wy0 = 1d0-wym
         cells(jx ,jy )= cells(jx ,jy ) + wx0*wy0
         cells(jxm,jy )= cells(jxm,jy ) + wxm*wy0
         cells(jxm,jym)= cells(jxm,jym) + wxm*wym
         cells(jx ,jym)= cells(jx ,jym) + wx0*wym
      end do
      end do
    end subroutine binrays
end module br

         
      

      
      
