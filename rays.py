# Magnification by ray shooting.

from numpy import *
from scipy import stats
import mynth        # Routines for integer factorization smooth numbers, etc.
import time         # For timing purposes

def keeptime(label):  # Keep track of time intervals
    global times, tlast
    tnow = time.clock()
    times.update({label:tnow-tlast})
    tlast = tnow
    #print '\nAt milepost '+label+' time.clock() = {:f}'.format(tnow)
    #print '....Elapsed wall-clock time is {:f}'.format(time.time()-twall0)
    
# Piecewise-parabolic spreading kernel for mass interpolation
def spread(u):
    au = abs(u)
    if au > 1.5 :
        return 0.
    elif au > 0.5 :
        return 0.5*(au-1.5)**2
    else :
        return 0.75 - au**2

# Apply the function spread() defined above to each element of an array
def spread_array(x):
    xshape = shape(x)
    y = x.flatten()
    for i in range(len(y)):
        y[i] = spread(y[i])
    return y.reshape(xshape)

# Interpolate stars onto mesh using PM method:
def mplace():
    # The kth star lies at x = xps[k], y = xps[k].
    # We distribute its mass over the 9 nearest lattice points
    # with weights chosen to give the correct total
    # mass and center of mass, and constant moments of inertia---i.e.,
    # constant variance of the density distribution.
    ix = rint(xps/dxi).astype(int)%nx
    iy = rint(yps/dyi).astype(int)%ny     
    # (ix[k],iy[k]) indexes the lattice point nearest to k^{th} star
    dix = xps/dxi - rint(xps/dxi)
    wxm = spread_array(1.+dix); wx0 = spread_array(dix); wxp = spread_array(1.-dix)
    # (wxm[k],wx0[k],wxp[k]) are weights for grid points at (ix[k]-1,ix[k],ix[k]+1) mod nx
    diy = yps/dyi - rint(yps/dyi)
    wym = spread_array(1.+diy); wy0 = spread_array(diy); wyp = spread_array(1.-diy)
    g = zeros((nx,ny))
    for k in range(nstar):
        xk = ix[k]; xkp = (xk+1)%nx
        yk = iy[k]; ykp = (yk+1)%ny
        mass = masses[k]
        g[xk-1,yk-1] += wxm[k]*wym[k]*mass        
        g[xk-1,yk  ] += wxm[k]*wy0[k]*mass
        g[xk-1,ykp ] += wxm[k]*wyp[k]*mass        
        g[xk ,yk-1]  += wx0[k]*wym[k]*mass        
        g[xk ,yk  ]  += wx0[k]*wy0[k]*mass
        g[xk ,ykp ]  += wx0[k]*wyp[k]*mass        
        g[xkp,yk-1]  += wxp[k]*wym[k]*mass        
        g[xkp,yk  ]  += wxp[k]*wy0[k]*mass
        g[xkp,ykp ]  += wxp[k]*wyp[k]*mass        

    #print 'star sum = {:e}; grid sum = {:e}. These should agree!'.format(sum(masses),sum(g)) #DEBUG

    return g

# Calculate forces exerted by the stars: Old version; constructs force kernel
# in Fourier space, then multiplies it by FT of density, then IFTs to real space.
def force_old():
    global poten,kx,ky,xwindow,ywindow,mgrid   # DEBUG
    g = fft.rfftn(mgrid)
    # Define the wavenumbers; the Nyquist wavenumber has ambiguous sign, so declare it zero:
    xNyq = (nx+1)//2 # If nx is odd, the Nyquist value is actually xNyq-0.5
    kx = zeros(nx)
    kx[:xNyq] = (2*pi/xsize) * arange(xNyq)
    for i in range(xNyq,nx):
        kx[i] = -kx[nx-i]
    yNyq = ny//2 # If ny is odd, the Nyquist index is actually yNyq+0.5
    ky = (2*pi/ysize) * arange(yNyq+1)
    if ny%2 == 0:
        ky[-1] = 0.
    # We don't need the negative ky's because we use a real-to-complex FFT (rfftn):
    # Use windows to suppress high-wavenumber power
    xwindow = 0.5*(1. + cos(kx*dxi))
    ywindow = 0.5*(1. + cos(ky*dyi))
    if nx%2 == 0:
        xwindow[nx/2] = 0.
    if ny%2 == 0:
        ywindow[ny/2] = 0.
    gshape = shape(g)
    gx = zeros(gshape,dtype=complex)
    gy = zeros(gshape,dtype=complex)    
    gxx = zeros(gshape,dtype=complex)
    gxy = zeros(gshape,dtype=complex)
    gyy = zeros(gshape,dtype=complex)
    fac = 2*pi/(dxi*dyi) # Poisson's equation is \nabla^2\phi = 2\pi \rho,
                         # so the factor 1/(cell size) is needed to convert
                         # the stellar masses to surface mass densities
    keeptime('windwave')
    for ix in range(nx):
         g[ix,:] = fac*g[ix,:]*xwindow[ix]*ywindow[:]/(kx[ix]**2 + ky[:]**2 + kminsq)
         # This is minus the FT of the smoothed gravitational potential
         gx[ix,:]  = (1.j)*(kx[ix]*g[ix,:])
         gy[ix,:]  = (1.j)*(ky[:]*g[ix,:])
         gxx[ix,:] = kx[ix]**2*g[ix,:]
         gyy[ix,:] = ky[:]**2*g[ix,:]
         gxy[ix,:] = kx[ix]*ky[:]*g[ix,:]
    keeptime('g')
    mshape = shape(mgrid)
    gx  = fft.irfftn(gx,s=mshape)
    gy  = fft.irfftn(gy,s=mshape)
    gxx  =  fft.irfftn(gxx,s=mshape)
    gyy  =  fft.irfftn(gyy,s=mshape)
    gxy  =  fft.irfftn(gxy,s=mshape)
    poten = fft.irfftn(g,s=mshape)  # DEBUG
    keeptime('FFT')
    jacob = g = (diagxx - gxx)*(diagyy - gyy) - gxy**2
    keeptime('jacob')
    return (gx,gy,jacob)

# Calculate forces exerted by stars: New version.  Constructs force kernel in real space,
# then uses FFTs to convolve with mass distribution.  Does not bother to compute jacobian or
# gravitational potential.  Notice also that it does not use global variables: the mass
# distribution is passed as an argument.
def force(mgrid,taper=True):
    keeptime('forces')
    # Assign x and y coordinates to grid
    x = mod(arange(xsize/2, 1.5*xsize,dxi), xsize) - xsize/2
    y = mod(arange(ysize/2, 1.5*ysize,dyi), ysize) - ysize/2    
    # With the above definitions, x ranges from -xsize/2 to (xsize/2)-dxi, and similarly for y.
    xx,yy = meshgrid(x,y,indexing='ij',sparse=True) # Older version of meshgrid doesn't support "indexing"
    #yy,xx = meshgrid(y,x,sparse=True)
    rm2 = xx**2 + yy**2
    rm2[0,0] = 1
    rm2 = 1./rm2  # rm2 now contains 1/r**2, where r is distance from the origin,
                  # except that rm2[0,0]=1 instead of 0; this is OK because we will set the
                  # Green's functions at the origin to zero anyway.
    ixh = nx/2 if nx%2 == 0 else (nx+1)/2
    iyh = ny/2 if ny%2 == 0 else (ny+1)/2    
    # Now that we have the radii, set xx and yy to zero at point half a periodicity length away:
    xx[ixh,:] = 0.;  yy[:,iyh] = 0.
    if taper == True: # Taper to suppress the discontinuities at half the periodicity length:
        xx = xx*cos(xx*pi/xsize)
        yy = yy*cos(yy*pi/ysize)
    # Compute the Green's functions, i.e., the forces exerted by a point mass at the origin, and
    # use DFTs to convolve these Green's functions with the mass distribution:
    mt = fft.rfftn(mgrid)
    fx = fft.irfftn(mt * fft.rfftn(-xx*rm2))
    fy = fft.irfftn(mt * fft.rfftn(-yy*rm2))
    return (fx,fy)

# Pairwise distances
def dists(x,y):
    n = len(x)
    d = zeros((n*(n-1))/2)
    k = 0
    for i in range(n):
        for j in range(i):
            d[k] = sqrt((x(i)-x(j))**2+(y(i)-y(j))**2)
            k += 1
    return d

# Shoot rays from grid points in image plane to source plane
def gridshot():
    i = arange(0,nx)
    j = arange(0,ny)
    J,I = meshgrid(j,i)
    xs = mod((diagxx*dxi)*I + fx[I,J], xsize_source)
    ys = mod((diagyy*dyi)*J + fy[I,J], ysize_source)
    return (xs,ys)

# Like gridshot, but oversampling by a factor 2 in each dimension:
def gridshot2():
    x = diagxx*linspace(0.,xsize,2*nx,endpoint=False)
    y = diagyy*linspace(0.,ysize,2*ny,endpoint=False)    
    ys,xs = meshgrid(y,x)  # Refined grid in source plane
    fh = 0.5*(fx + roll(fx,-1,0)) # Forces interpolated along x
    xs[0::2,0::2] += fx
    xs[1::2,0::2] += fh
    xs[0::2,1::2] += 0.5*(fx + roll(fx,-1,1))
    xs[1::2,1::2] += 0.5*(fh + roll(fh,-1,1))
    fh = 0.5*(fy + roll(fy,-1,0))
    ys[0::2,0::2] += fy
    ys[1::2,0::2] += fh
    ys[0::2,1::2] += 0.5*(fy + roll(fy,-1,1))
    ys[1::2,1::2] += 0.5*(fh + roll(fh,-1,1))
    return (xs,ys)
# Like gridshot2, but binning the data in the source plane into cells,
# and returning the cell counts only
def gridshot3(nxs,nys):
    x = diagxx*linspace(0.,xsize,2*nx,endpoint=False)
    y = diagyy*linspace(0.,ysize,2*ny,endpoint=False)    
    ys,xs = meshgrid(y,x)  # Refined grid in source plane
    fh = 0.5*(fx + roll(fx,-1,0)) # Forces interpolated along x
    xs[0::2,0::2] += fx
    xs[1::2,0::2] += fh
    xs[0::2,1::2] += 0.5*(fx + roll(fx,-1,1))
    xs[1::2,1::2] += 0.5*(fh + roll(fh,-1,1))
    fh = 0.5*(fy + roll(fy,-1,0))
    ys[0::2,0::2] += fy
    ys[1::2,0::2] += fh
    ys[0::2,1::2] += 0.5*(fy + roll(fy,-1,1))
    ys[1::2,1::2] += 0.5*(fh + roll(fh,-1,1))
    fh = 0  # To conserve memory
    print 'About to make histogram' #DEBUG
    a = xsize_source; b = ysize_source
    xs = xs.flatten()%xsize_source
    ys = ys.flatten()%ysize_source    
    h,xedges,yedges = histogram2d(xs,ys,bins=[nxs,nys],range=[[0.,a],[0.,b]])
    return h

# Group the rays into a grid of nx by ny cells in source plane
def binrays(x,y,nx,ny):
    dx = xsize_source/nx
    dy = ysize_source/ny
    ix = (rint(x/dx).astype(int)%nx).flatten()
    iy = (rint(y/dy).astype(int)%ny).flatten()
    cells = zeros((nx,ny),dtype='int')
    for k in range(len(ix)):
        cells[ix[k],iy[k]] += 1.
    return cells
# Like binrays, but spreading the mass over the four nearest vertices.
def binrays2(x,y,nx,ny):
    dx = xsize_source/nx
    dy = ysize_source/ny
    fx = (x/dx).flatten(); ix = ceil(fx).astype(int)
    fy = (y/dy).flatten(); iy = ceil(fy).astype(int)
    cells = zeros((nx,ny))
    for k in range(len(ix)):
        jx = ix[k]%nx; jy = iy[k]%ny
        wxm = ix[k]-fx[k]; wx0 = 1.-wxm
        wym = iy[k]-fy[k]; wy0 = 1.-wym        
        cells[jx,jy]     += wx0*wy0
        cells[jx-1,jy]   += wxm*wy0
        cells[jx-1,jy-1] += wxm*wym
        cells[jx,jy-1]   += wx0*wym
    return cells
# Group the rays into a grid of nx by ny cells in source plane;
# this version uses histogram2d
def binrays3(nx,ny):
    a = xsize_source; b = ysize_source
    h,xedges,yedges = histogram2d(xs.flatten()%a,ys.flatten()%b,bins=[nx,ny],range=[[0.,a],[0.,b]])
    return h

# Estimate jacobian by finite differencing potential
def jake(p):
    jest = zeros((nx,ny))
    for ix in range(nx):
        ip = (ix+1)%nx; im = (ix-1)%nx
        for jy in range(ny):
            jp = (jy+1)%ny; jm = (jy-1)%ny
            pxx = (p[ip,jy]-2*p[ix,jy]+p[im,jy])/dxi**2
            pyy = (p[ix,jp]-2*p[ix,jy]+p[ix,jm])/dyi**2
            pxy = (p[ip,jp]-p[im,jp]+p[im,jm]-p[ip,jm])/(4*dxi*dyi)
            jest[ix,jy] = (diagxx+pxx)*(diagyy+pyy)-pxy**2
    return jest

# Estimate jacobian by finite differencing forces
def fake(fx,fy):
    jest = zeros((nx,ny))
    for ix in range(nx):
        for jy in range(ny):
            pxx = (fx[ix,jy]+fx[ix,jy-1]-fx[ix-1,jy]-fx[ix-1,jy-1])/(2*dxi)
            pxy = (fx[ix,jy]+fx[ix-1,jy]-fx[ix,jy-1]-fx[ix-1,jy-1])/(2*dyi)
            pyy = (fy[ix,jy]+fy[ix-1,jy]-fy[ix,jy-1]-fy[ix-1,jy-1])/(2*dyi)
            jest[ix,jy] = (diagxx+pxx)*(diagyy+pyy)-pxy**2
    return jest


#############################################################################################
# Immediate statements
############################################################################################

# kappa      = double(raw_input(' Enter total convergence, kappa: '))
# kappa_star = double(raw_input(' Input stellar convergence, kappa_star: '))
# gamma      = double(raw_input(' Enter shear, gamma: '))
# sigma0     = double(raw_input(' Enter Miller-Scalo mass dispersion [dex] : '))
# nfft       = int(raw_input(' Enter FFT size: '))
# xsize      = double(raw_input(' Enter size of image plane in Einstein ring radii: '))
#[kappa,kappa_star,gamma,sigma0,nfft,xsize] = [0.45,0.045,0.45,0.3,4096,256.] # use fewer pixels in x, but more in y: see "nyt =" below
#[kappa,kappa_star,gamma,sigma0,nfft,xsize] = [0.45,0.045,0.45,0.3,2048,256.]
# [kappa,kappa_star,gamma,sigma0,nfft,xsize] = [0.45,0.045,0.45,0.3,16384,1024.]
[kappa,kappa_star,gamma,sigma0,nfft,xsize] = [0.45,0.045,0.45,0.3,16384,512.]
#[kappa,kappa_star,gamma,sigma0,nfft,xsize] = [0.395,0.395,0.395,0.5,4096,152.] # Q2237, image A

# Derived quantities
sigma    = sigma0*log(10.)       # Dispersion of natural logs of stellar masses
m0       = exp(-0.5*sigma**2)    # Modal mass, in units where average mass = 1.
kappa_c  = kappa - kappa_star    # Convergence of nonstellar mass
diagxx   = 1.-kappa-gamma        # First eigenvalue of macroimaging matrix
diagyy   = 1.-kappa+gamma

# Set the x and y sizes of the image plane so that its projection into
# the source plane is approximately square
aspect = abs(diagxx/diagyy)
ysize = xsize*aspect
xsize_source = diagxx*xsize
ysize_source = diagyy*ysize

# Set the numbers of pixels in each direction on the image plane,
# hewing as closely as possible to the aspect ratio, while using smooth
# numbers for efficient FFTs.  So the image pixels will be nearly square.
nx = nfft
#nyt = aspect*nx
nyt = sqrt(aspect)*nx  # Compromise on the number of pixels in each dimension
ny  = mynth.last_smooth(floor(nyt))
ny2 = mynth.next_smooth(ceil(nyt))
if (ny2/nyt -1.) < (1.- ny/nyt) :
    ny = ny2
dxi = xsize/nx
dyi = ysize/ny
dxs = xsize_source/nx
dys = ysize_source/ny
print 'aspect ratio = {:f}, xsize = {:f}, ysize = {:f}, dxi={:f}, dyi={:f}'.format(aspect,xsize,ysize,dxi,dyi) #DEBUG
print 'Image plane will have {:d} by {:d} pixels'.format(nx,ny)

# Set the square of the cutoff wavenumber.
# 2*pi/sqrt(kminsq) is the "screening length" of the gravitational
# potential, which should be less than the periodicity length but
# large compared to the Einstein-ring radius
kminsq = (2*pi/min(xsize,ysize))**2

# Choose the number, masses, and positions of the stars
rings  = xsize*ysize/pi     # Area of image plane divided by that of one Einstein ring
nstar  = int(round(kappa_star*rings/m0))
masses = m0*exp(stats.norm.rvs(loc=0.,scale=sigma,size=nstar))
xps = stats.uniform.rvs(loc=0.,scale=xsize,size=nstar)
yps = stats.uniform.rvs(loc=0.,scale=ysize,size=nstar)

# DEBUG: Use a single mass
#masses = array([1.])
#xps = array([xsize/2])
#yps = array([ysize/2])
#diagxx = 0.5
#diagyy = 2.
#nstar = 1

# Start the clock
tlast = time.clock()
times = dict([('start',0.)])
twall0 = time.time()

# Distribute the masses on the grid
mgrid = mplace()
keeptime('mplace')
# Calculate the forces with FFTs
#[fx,fy,jacob] = force()
fx,fy = force(mgrid)  # New version of force() routine.
[nfx,nfy] = shape(fx)  #DEBUG
# Shoot rays from grid points:
[xs,ys] = gridshot2()
#cells = gridshot3(nx,ny) # Alternative version, using "histogram": not nearly as fast as gridshot2() followed by
                          # the fortran routine br.br.binrays(...)
keeptime('gridshot')
fx = fy = 0  # To save memory

# Exemplary interactive commands to run at this point,
#    presuming that we've already said "import rays as r":
# >> cells = r.binrays2(r.xs,r.ys,r.nx,r.ny)
# Better yet, if you've built the fortran module br (based on binrays.f90):
# >> import br
# >> cells = br.br.binrays(r.xs,r.ys,r.xsize_source,r.ysize_source,r.nx,r.ny)/4
#
# The above is much faster.  Next, normalize to one ray per cell on average:
# >> cells = cells /cells.mean()  # Normalize to one ray per cell on average.
#
# Now make a picture:
# >> from scipy import ndimage
# >> ratio = r.dys/r.dxs
# >> smoothing = 0.5*array([r.dys/r.dxs,1.]) # Compensate for non-square pixels
# >> smag  = ndimage.gaussian_filter(cells,sigma=smoothing,mode='wrap') # "mode='wrap'" invokes periodicity
# >> img = imshow(smag.transpose(),origin='lower',extent=(0.,r.xsize_source,0.,r.ysize_source)) # Note transpose
# >> colorbar()
