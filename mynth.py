# Some basic factorization routines

def factor(n):
    if isinstance(n,int) != True:
        print str(n) + ' is not an integer'
        return False
    an = abs(n)
    list = []
    if an <= 3:
        list.append(n)
        return [an]
    else:
        k = 2; dk = 1
        while k*k <= an :
            if an % k == 0:
                an = an/k
                list.append(k)
            else:
                k += dk; dk = 2
        if an > 1:
            list.append(an)
        return list

def ismooth(n,maxfac=7):
    if factor(n)[-1] > maxfac :
        return False
    else:
        return True

def next_smooth(x,maxfac=7):
    n = int(round(x))
    for k in range(n,2*n):
        if ismooth(k,maxfac):
            return k

def last_smooth(x,maxfac=7):
    n = int(round(x))
    for k in range(n,0,-1):
        if ismooth(k,maxfac):
            return k

            
